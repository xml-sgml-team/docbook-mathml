<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xhtml="http://www.w3.org/1999/xhtml" version="1.0">
<!-- ugly script to get all dirs -->
  <xsl:output method="text" indent="no"/>
  <xsl:template match="/">
    <xsl:for-each select="//td/img[@alt = '[DIR]' and @src='/icons/folder.gif']">
      <xsl:value-of select="../../td/a[@href]"/>
      <xsl:text>
</xsl:text>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
